// 1. 简述react生命周期以及每个生命周期的作用，使用自己的话描述出来。
        // 初始化
            // 1、getDefaultProps() 设置默认的props，也可以用dufaultProps设置组件的默认属性.
            // 2、getInitialState() 在使用es6的class语法时是没有这个钩子函数的，可以直接在constructor中定义this.state。此时可以访问this.props
            // 3、componentWillMount() 组件初始化时只调用，以后组件更新不调用，整个生命周期只调用一次，此时可以修改state。
            // 4、 render() react最重要的步骤，创建虚拟dom，进行diff算法，更新dom树都在此进行。此时就不能更改state了。
            // 5、componentDidMount() 组件渲染之后调用，只调用一次。
        // 更新
        // 6、componentWillReceiveProps(nextProps) 组件初始化时不调用，组件接受新的props时调用。
        // 7、shouldComponentUpdate(nextProps, nextState) react性能优化非常重要的一环。组件接受新的state或者props时调用，我们可以设置在此对比前后两个props和state是否相同，如果相同则返回false阻止更新，因为相同的属性状态一定会生成相同的dom树，这样就不需要创造新的dom树和旧的dom树进行diff算法对比，节省大量性能，尤其是在dom结构复杂的时候
        // 8、componentWillUpdata(nextProps, nextState) 组件初始化时不调用，只有在组件将要更新时才调用，此时可以修改state
        // 9、render() 组件渲染
        // 10、componentDidUpdate()
        // 卸载
        // 11、componentWillUnmount()组件将要卸载时调用，一些事件监听和定时器需要在此时清除。
        // 即将废弃的勾子    现在使用会出现警告，下一个大版本需要加上UNSAFE_前缀才能使用，以后可能会被彻底废弃，不建议使用。
            // componentWillMount
            // componentWillReceiveProps
            // componentWillUpdate


            // 2. 函数组件和class的组件的区别
                // 函数式组件没有this，没有自己的状态 ，也没有生命周期 只有props 渲染DOM，而不关注其他逻辑。
                // 类组件
                // 1. 组件定义方式不同；
                // 2. （因为组件定义方式不同）生命周期不同：class组件有，函数组件无；
                // 3. （因为生命周期不同）副作用操作执行不同：class组件通过生命周期函数，函数组件用hook的useEffect；
                // 4. state的定义、读取、修改方式不同：函数组件用hook的useState；
                // 5. this：class组件有，函数组件无；
                // 6. 实例：class组件有，函数组件无；
                // 7. （因为实例不同）ref使用不同：class组件有，函数组件无；



                // 3. this.setState 回调函数什么时候执行