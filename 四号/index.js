// 发布订阅
// 发布-订阅也叫观察者模式，在javascript中常应用为事件监听。
// 2.实现发布订阅模式的步骤
// 1.确定充当发布者的对象 2.然后给发布者添加一个缓存列表，里面放入回调函数 3.当事件发生时，遍历并触发订阅这个事件的缓存列表
// 案例：书店为了减少库存，只有在消费者付了订金订某本书后，才会去批发商那里进货，进货后会通知订书的买家来拿

 
var BookProvider = {}
BookProvider .clientList = {}
BookProvider .listener = function(key, fn){
  if (!this.clientList[key]){
    this.clientList[key] = []
  }
  this.clientList[key].push(fn)
}
BookProvider .trigger = function(){
  var args = [].slice.apply(arguments)
  var key = args.shift()
  var that = this
  
  if (!this.clientList[key] || !this.clientList[key].length){
    return false
  }
  this.clientList[key].forEach(function(fn){
    fn.apply(that, args)
  })
}

//调用
BookProvider .listener('javascript设计模式', function(msg){console.log('小明:'+msg)})
BookProvider .listener('javascript设计模式', function(msg){console.log('小红:'+msg)})
BookProvider.trigger('javascript设计模式', '付钱拿书')

//通用订阅发布模式，让普通的对象获得被订阅和发布的能力
function initEvent(o){
    var event = {}
    function listener(key, fn){
        if (!event[key]){
            event[key]= []
        }
        event[key].push(fn)
    }

    function trigger(...args){
        var key = args.shift()
        var fns = event[key]
        if(!fns){
            return false
        }
    
        fns.forEach(f => {
            f.apply(this, args)
        })
    }

    function removeListener(key, fn){
        var fns = event[key]
        if (!fns){
            return false
        }
    
        if (!fn){
            fns.length = 0 //不传fn就把该事件下的所有监听函数删除
        }else {
            for (var i = fns.length - 1; i >= 0; i--){
                if (fns[i]== fn){
                    fns.splice(i, 1)
                }
            }
        }
    }
    return Object.assign(o, {
        listener,
        trigger,
        removeListener
    })
}

// 深浅拷贝
    //  浅拷贝    新的对象复制已有对象中非对象属性的值和对象属性的引用就是浅拷贝。
    var obj = {x: 1,y: 2,a: {z: 3}}
    var obj1 = Object.assign({}, obj)
    obj.a.z = 4
    obj.x = 2
    console.log(obj) // {x: 2, y: 2,  a: {z: 4}
    console.log(obj1) // {x: 1, y: 2, a: {z: 4}}
    // Object.assign()  ||    ...解构赋值
    // 深拷贝
        //使用递归的方式实现数组、对象的深拷贝
        function deepClone(obj) {
          //判断拷贝的要进行深拷贝的是数组还是对象，是数组的话进行数组拷贝，对象的话进行对象拷贝
          var objClone = Array.isArray(obj) ? [] : {};
          //进行深拷贝的不能为空，并且是对象
          if (obj && typeof obj === "object") {
            for (key in obj) {
              if (obj.hasOwnProperty(key)) {
                if (obj[key] && typeof obj[key] === "object") {
                  objClone[key] = deepClone(obj[key]);
                } else {
                  objClone[key] = obj[key];
                }
              }
            }
          }
          return objClone;
        }


//通过js的内置对象JSON来进行数组对象的深拷贝
      function deepClone(obj) {
        var _obj = JSON.stringify(obj),
          objClone = JSON.parse(_obj);
        return objClone;
      }

      //  通过jQuery的extend方法
              // var array = [1,2,3,4];
              // var newArray = $.extend(true,[],array);
      // Object.assign()
      // 对象中只有一级属性，没有二级属性的时候，为深拷贝
      // 对象中有对象的时候，在二级属性以后就是浅拷贝

      // 4. 解析 URL 参数为对象

          function parseParam(url) {
            // 补全代码
            let name, value;
            let num = url.indexOf("?");
            str = url.substr(num + 1); //取得所有参数
            let arr = str.split("&"); //各个参数放到数组里
            let json = {};
            for (let i = 0; i < arr.length; i++) {
              num = arr[i].indexOf("=");
              if (num > 0) {
                name = arr[i].substring(0, num);
                value = arr[i].substr(num + 1);
                json[name] = value;
              }
            }
            return json;
        }
        
        console.log(parseParam("http://www.baidu.com?name=yihang&password=123456"));
        //{name: 'yihang', password: 123456}