
// 1. 简述一下对promise的理解
//  这种做法在逻辑比较复杂的回调嵌套中会相当复杂；也叫做回调地狱；
//  ​ promise用来将这种繁杂的做法简化，让程序更具备可读性，可维护性；
//  ​ promise内部有三种状态，pedding，fulfilled，rejected；
//  ​ pedding表示程序正在执行但未得到结果，即异步操作没有执行完毕，fulfilled表示程序执行完毕，且执行成功，
//  ​ rejected表示执行完毕但失败；这里的成功和失败都是逻辑意义上的；并非是要报错。
//  ​ 其实，promise和回调函数一样，都是要解决数据的传递和消息发送问题，
//  ​ promise中的then一般对应成功后的数据处理，catch一般对应失败后的数据处理
//  版权声明：本文为CSDN博主「该睡觉觉了」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。


// 2. 手写实现promise

const PENDING = "pending"
const FULFILLED = "fulfilled"
const REJECTED = "rejected"

function Promise(fn) {
    let that = this
    this.status = PENDING
    this.value = undefined
    this.reason = undefined
    this.resolvedCb = []
    this.rejectedCb = []

    let resolve = function (value) {
        that.value = value
        that.status = FULFILLED
        that.resolvedCb.forEach(cb => cb(that.value))
    }
    let reject = function (reason) {
        that.reason = reason
        that.status = REJECTED
        that.rejectedCb.forEach(cb => cb(that.reason))
    }

    try {
        fn(resolve, reject)
    } catch (e) {
        reject(e)
    }
}

Promise.prototype.then = function (onFulfilled, onRejected) {
    onFulfilled = onFulfilled instanceof Function ? onFulfilled : () => {}
    onRejected = onRejected instanceof Function ? onRejected : () => {}

    let that = this

    if (this.status === FULFILLED) {
        return new Promise((resolve, reject) => {
            if (this.value instanceof Promise) {
                this.value.then(onFulfilled, onRejected)
            } else {
                try {
                    resolve(onFulfilled(this.value))
                } catch (e) {
                    reject(e)
                }
            }
        })
    }
    if (this.status === REJECTED) {
        return new Promise((resolve, reject) => {
            try {
                resolve(onRejected(this.reason))
            } catch (e) {
                reject(e)
            }
        })
    }
    if (this.status === PENDING) {
        return new Promise((resolve, reject) => {
            that.resolvedCb.push(() => {
                if (this.value instanceof Promise) {
                    this.value.then(onFulfilled, onRejected)
                } else {
                    try {
                        resolve(onFulfilled(this.value))
                    } catch (e) {
                        reject(e)
                    }
                }
            })
            that.rejectedCb.push(() => {
                try {
                    resolve(onRejected(this.reason))
                } catch (e) {
                    reject(e)
                }
            })
        })
    }
}


// 3. 简述一下promise.all的使用方法，补全以下代码：
Promise.myAll = function(arr){
  return arr(function(resolve,reject) {
    //promises必须是一个数组
        if(!(arr instanceof Array)) {
            throw new TypeError("arr must be an Array");
        }
        var len = arr.length,
            resolvedCount = 0,
            resolvedArray = new Array(len);
        for(var i = 0;i < len ;i++) {
            (function(i) {
                Promise.resolve(arr[i])
                    .then(value => {
                        resolvedCount++;
                        resolvedArray[i] = value;
                        if(resolvedCount == len) {
                            return resolve(resolvedArray);
                        }
                    },re => {
                        return reject(re);
                    })
                    .catch(re => {
                        console.log(re);
                    })              
            })(i)
        }
    })
}

Promise.myAll([Promise.resolve("1"),Promise.resolve("2")]).then(res => {
    console.log(res) // ["1","2"]
})

// 4. 简述一下promise.race的使用方法, 补全以下代码：
Promise.myRace = function(arr){
  return arr((resolve, reject) => {
    // 谁返回的结果最快 就用谁的
    for (let i = 0; i < arr.length; i++) {
        let current = arr[i];
        if (isPromise(current)) {
            current.then(resolve, reject)
        } else {
            resolve(current);
        }
    }
})
}
const getStatus = () => new Promise(function (resolve, reject) {
    setTimeout(() => {
        if(Math.random() > 0.5){
            resolve("success");
            return;
        }
        reject("fail");
    }, 5000)
});
Promise.myRace([getStatus(),getStatus()]).then(res => {
    console.log(res);
}).catch(error => {
    console.log(error);
})